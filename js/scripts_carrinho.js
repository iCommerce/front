console.log("A página está sendo carregada");

let produtos = [];

let dados;

let resposta;

$(document).ready(function() {
	if(localStorage.getItem("id_sessao")==null)
	{
		localStorage.setItem("id_sessao",new Date().getTime().toString());
	}
});

let idSessao = localStorage.getItem("id_sessao");

function carregarProdutosNoCarrinho(){
  fetch('http://104.131.164.162:8083/carrinho/'+idSessao, {
  method: 'GET'
}).then(resposta => {
  return resposta.json();
}).then(resposta => {

  let lista = document.getElementById('produtos_list');

  let total = document.getElementById('total_cart');

lista.innerHTML="";
total.innerHTML="";
	resposta.itemCarrinho.forEach(item => {
		lista.innerHTML += "<tr class=\"cart_item\"><td class=\"cart-product-remove\"><a href=\"#\" onclick=\"remove_carrinho("+item.id+")\" class=\"remove\" title=\"Remove this item\"><i class=\"icon-trash2\"></i></a></td><td class=\"cart-product-thumbnail\"><a href=\"#\"><img width=\"64\" height=\"64\" src=\""+item.imagem+"\" alt=\""+item.nome+"\"></a></td><td class=\"cart-product-name\"><a href=\"#\">"+item.nome+"</a></td><td class=\"cart-product-price\"><span class=\"amount\">R$"+item.preco.toFixed(2)+"</span></td></tr>"
		});

	lista.innerHTML +="<tr class=\"cart_item\">	<td colspan=\"6\">	<div class=\"row clearfix\"><div class=\"col-lg-8 col-8 nopadding\"><a href=\"#\" class=\"button button-3d nomargin fright\">Update Cart</a><a href=\"checkout.html\" class=\"button button-3d notopmargin fright\">Proceed to Checkout</a></div></div></td></tr>"
  total.innerHTML = "<td class=\"cart-product-name\"><strong>Total</strong></td><td class=\"cart-product-name\"><span class=\"amount color lead\"><strong>R$"+resposta.valorTotal.toFixed(2)+"</strong></span></td>"

});
}


//let botaoProduto = document.querySelector(".view-cart");
//if(botaoProduto != null){
//  botaoProduto.onclick = carregarProdutosNoCarrinho;
//}

function abrirPaginaCarrinho(){
  window.location.href = "cart.html";
  }
